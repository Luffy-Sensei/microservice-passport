package com.tenor.sf.jobflow.passport.dao;


import com.tenor.sf.jobflow.passport.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

}
