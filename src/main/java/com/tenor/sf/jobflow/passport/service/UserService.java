package com.tenor.sf.jobflow.passport.service;

import com.tenor.sf.jobflow.passport.dao.UserRepository;
import com.tenor.sf.jobflow.passport.domain.User;
import io.leangen.graphql.annotations.GraphQLArgument;
import io.leangen.graphql.annotations.GraphQLMutation;
import io.leangen.graphql.annotations.GraphQLQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private final UserRepository repository;

    @Autowired
    public UserService(UserRepository repository) {
        this.repository = repository;
    }

    @GraphQLQuery(name = "findAll")
    public List<User> findAll() {
        return repository.findAll();
    }

    @GraphQLQuery(name = "getUserById")
    public Optional<User> getUserById(@GraphQLArgument(name = "id") Long id) {
        return repository.findById(id);
    }

    @GraphQLMutation(name = "saveUser")
    public User saveUser(@GraphQLArgument(name = "user") User user) {
        return repository.save(user);
    }

    @GraphQLMutation(name = "deleteUser")
    public void deleteUser(@GraphQLArgument(name = "id") Long id) {
        repository.deleteById(id);
    }
}
