package com.tenor.sf.jobflow.passport.domain;

import io.leangen.graphql.annotations.GraphQLQuery;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ToString
public class User {

    @Id
    @GeneratedValue
    @GraphQLQuery(name = "id", description = "User's id")
    private Long id;

    @GraphQLQuery(name = "login", description = "User's login")
    @Column(nullable = false)
    private String login;

    @GraphQLQuery(name = "password", description = "User's password")
    @Column(nullable = false)
    private String password;

    @GraphQLQuery(name = "creationDate", description = "The creation date of the user")
    private LocalDate creationDate;
}
